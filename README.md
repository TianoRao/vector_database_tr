# Simple REST API Semantic and Vector Database using Qdrant

## Purpose of Project

This project creates a simple REST API that performs semantic search on a vector database. It ingests data into the vector database, performs queries based on user input, and visualizes the output. Specifically, it finds the most similar singer related to a user's query sentence. This project combines elements from previous work to provide easy access to a vector database via a GET request, allowing users to interact with the database through a public endpoint via an API gateway.

## Video
[Youtube Link](https://youtu.be/MhornE3EdVA)

## Getting Started

### Prerequisites

- Rust programming environment.
- Docker installed on your machine.
- An AWS account with permissions to use Lambda, API Gateway, and ECR services.
- Qdrant and Cohere accounts for accessing their APIs.

### Setup

1. **Qdrant and Cohere Accounts**:
   - Sign up for Qdrant and create a cluster to access a vector database. Note down your API key and cluster URL.
   - Sign up for Cohere and obtain an API key for generating embeddings for the vector database.

2. **Rust Project**:
   - Create a new Rust project for Lambda:
     ```bash
     cargo lambda new <folder_name>
     ```
   - Store your API keys (Qdrant, Cohere, AWS) in a `.env` file. Ensure this file is added to `.gitignore` to avoid pushing sensitive information to your repository.

3. **Data Ingestion**:
   - Use the `setup.rs` script to ingest data into your Qdrant collection. Adjust the script to read from a `.jsonl` file of various animals and pass this data for ingestion.
   - Run your data ingestion script. Example:
     ```bash
     cargo run --bin setup_collection animals.jsonl
     ```

4. **Testing and Deployment**:
   - To test locally, use:
     ```bash
     cargo lambda watch
     ```
   - Test your API locally by sending a GET request, e.g., `http://localhost:9000/?query='rockstar'`.
   - Set up an IAM user with the necessary permissions (`IAMFullAccess`, `AmazonEC2ContainerRegistryFullAccess`, `AWSLambda_FullAccess`) and generate an access key for deployment.
   - Build your project for release:
     ```bash
     cargo lambda build --release
     ```
   - Deploy your project:
     ```bash
     cargo lambda deploy --env-file .env
     ```

5. **ECR and Lambda Setup**:
   - Create a Docker image for your project and push it to AWS ECR.
   - Use the AWS Lambda console to create a Lambda function from your ECR image, ensuring the environment variables are correctly set.

6. **API Gateway**:
   - Link your Lambda function with AWS API Gateway to create a public endpoint for your API. Enable CORS if needed.

## Usage

To use the API, send a GET request to the deployed API Gateway URL with the query parameter. For example:

https://dokt98yojl.execute-api.us-east-1.amazonaws.com/mini7/mini7?test_key=rockstar

The API will return the most similar singer related to the query sentence.

## Screenshots
1. Vector Dataset on Qdrant
![Screenshot_2024-03-24_at_5.36.51_PM](/uploads/a4b65f4d82ccdd9f046493c0d690070b/Screenshot_2024-03-24_at_5.36.51_PM.png)


2. Get result via REST API
![Screenshot_2024-03-24_at_5.37.44_PM](/uploads/01434abdffd93de95d5608fd656e15c3/Screenshot_2024-03-24_at_5.37.44_PM.png)

3. ECR
![Screenshot_2024-03-24_at_5.38.29_PM](/uploads/ef610dad20895c21998c899a71442724/Screenshot_2024-03-24_at_5.38.29_PM.png)

4. CICD
![Screenshot_2024-03-24_at_6.11.18_PM](/uploads/ca64d49215d0a51d6e4026fc80b6f117/Screenshot_2024-03-24_at_6.11.18_PM.png)


5. Lambda functions
![Screenshot_2024-03-24_at_5.40.26_PM](/uploads/57357fbb8fa9ccbcd0f2c0626ab2b920/Screenshot_2024-03-24_at_5.40.26_PM.png)

![Screenshot_2024-03-24_at_5.40.35_PM](/uploads/85a975169de736631ba2029c0e452d98/Screenshot_2024-03-24_at_5.40.35_PM.png)

## References

- Qdrant Rust Client: https://github.com/qdrant/rust-client
- Cohere API Documentation: https://docs.cohere.com/docs/the-cohere-platform
- AWS Lambda with Rust Guide: https://medium.com/@jed.lechner/effortless-guide-to-setting-up-aws-lambda-with-rust-b2630eeaa0f0

For detailed instructions on setting up the environment, data ingestion, and deployment steps, refer to the links provided in the References section.





